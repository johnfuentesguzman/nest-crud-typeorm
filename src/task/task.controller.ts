import { GetDataFilterDto}  from '../task/dto/get-task-filter.dto'
import { CreateTaskDto} from '../task/dto/create-task.dto'
import { TaskService } from './task.service';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { TaskStatusValidationPipe } from './pipes/task-status-validation.pipe';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';

@Controller('task')
export class TaskController {
    constructor( private taskService: TaskService) {}
    
    @Get()
    // ValidationPipe takes the custom pipe task-status-validation.pipe.ts
    getTasks(@Query(ValidationPipe) filterDto: GetDataFilterDto): Promise<Task[]> {
      return this.taskService.getTasks(filterDto);
    }
  
    @Get('/:id')
    getTaskById(@Param('id', ParseIntPipe) id: number): Promise<Task> {
      return this.taskService.getTaskById(id);
    }
  
    @Post()
    @UsePipes(ValidationPipe) // takes the custom pipe task-status-validation.pipe.ts
    
    createTask(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
      return this.taskService.createTask(createTaskDto);
    }
  
    @Delete('/:id')
    deleteTask(@Param('id', ParseIntPipe) id: number): Promise<void> {
      return this.taskService.deleteTask(id);
    }
  
    @Patch('/:id/status')
    updateTaskStatus(
      @Param('id', ParseIntPipe) id: number,
      @Body('status', TaskStatusValidationPipe) status: TaskStatus,
    ): Promise<Task> {
      return this.taskService.updateTaskStatus(id, status);
    }

}
